/**
 * Function to provide to the user a message indicating the sales rep email.
 * It will provide an alert with the email of the sales rep.
 *
 * @function clientPostSourcing
 *
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @returns {undefined}
 *
 **/
function clientPostSourcing(type, name) {
   if(isEntity(name)) {
	   // get the value for the sales rep
	   var sales_rep = nlapiGetFieldText("salesrep");

	   // display the alert message
	   var message = messageSalesRep(sales_rep)

	   if(message !== "")
		   alert(message);
   }
}

/**
 * Receives a name if the name is entity return true otherwise returns false.
 * @function isEntity
 * @param  {String}  name The name of the field in this case.
 * @return {Boolean} Returns true if name === "entity", false in other cases.
 */
function isEntity(name) {
   return name === "entity"
}

/**
 * Returns the message if the sales rep field has a value or an empty string if not.
 * @function messageSalesRep
 * @param  {String} sales_rep The sales rep value for the field.
 * @return {String} Returns the message to present for the customer.
 */
function messageSalesRep(sales_rep) {
   var message = "";
   if(sales_rep) {
      message = "Please remember mail to " + sales_rep + " about the order";
   }
   return message;
}
