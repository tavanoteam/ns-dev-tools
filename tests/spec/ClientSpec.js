describe("Sales Rep functionality", function() {

  it("should run only for entity field", function() {
    var is_entity = isEntity("entity");
    expect(is_entity).toBe(true);
  });

  it("should not run for other field than entity", function() {
    var is_entity = isEntity("date");
    expect(is_entity).not.toBe(true);
  });

  it("should display the sales rep if is not null or empty", function() {
    var message = messageSalesRep("salesrep@email.com");
    expect(message).not.toBe("");
  });

  it("should return an empty string if no sales rep", function() {
     var message = messageSalesRep("");
    expect(message).toBe("");
  });
});
